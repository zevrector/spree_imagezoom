//= require frontend/jqzoom
add_image_handlers = function() {};
$(document).ready(function(){
  var options = {
            zoomType: 'innerzoom',
            preloadImages: true,
            zoomWidth: 600,
            zoomHeight: 600,
            showEffect : 'fadein',
            hideEffect: 'fadeout'
  };
  $('.spree_jqzoom').jqzoom(options);
});
